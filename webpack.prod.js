const path = require('path');
const common = require("./config/webpack.common");
const merge = require("webpack-merge");

const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const buildTarget = 'dist';

module.exports = merge(common, {

    module:{
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(svg|png|jpg|gif)$/,
                use:{
                    loader: 'file-loader',
                    options: {
                        name: '[name]-[hash].[ext]',
                        outputPath: '../img',
                        publicPath: 'img'
                    }
                }
            }
        ]
    },

    plugins: [
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [path.resolve(__dirname, './' + buildTarget)]
        }),
        new MiniCssExtractPlugin({
            filename: '../css/[name]-[hash].bundle.css',
        }),
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: '../index.html',
            chunks: ['vendor', 'app'],
            minify: {
                removeAttributeQuotes: true
            }
        })
    ],

    optimization: {
        splitChunks: { /* Split or Chunk Js files into seprate App and Library specific bundle files */
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'initial'
                }
            }
        },

        minimizer: [ /* Minify CSS and JS bundled files*/
            new OptimizeCssAssetsPlugin(),
            new TerserPlugin(),
        ]
    },

    output: { /* chunkhash used for providing separate hashes to vendor and app js files based on changes*/
        filename: '[name]-[chunkhash].bundle.js',
        chunkFilename: '[name]-[chunkhash].bundle.js',
        path: path.resolve(__dirname, './' + buildTarget + '/js')
    }
});