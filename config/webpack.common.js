const Webpack = require('webpack');

module.exports = {
    entry: {
        app: './src/index.js'
    },

    module:{
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(html)$/,
                use:{
                    loader: 'html-loader'
                }
            }
        ]
    },

};