import './style/main.scss';
import Form from './js/component/Form';

import "core-js/stable";
import "regenerator-runtime/runtime";

// /* Asynchronous Javascript using fetch API */
// fetch('https://api.randomuser.me/?nat=US&results=1')
// .then(res => res.json())
// .then(json => json.results)
// .then(console.log)
// .catch(console.error);

// /* Asynchronous Javascript using aync / await */

// const getFakeProfile = async () => {
//     try{
//         let res = await fetch('https://api.randomuser.me/?nat=US&results=1');
//         let { results } = res.json();
    
//         console.log(`Asyn/Await API : ${results}`);
//     }catch(err){
//         console.error(err);
//     }
// }

// getFakeProfile();