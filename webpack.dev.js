const path = require('path');
const common = require("./config/webpack.common");
const merge = require("webpack-merge");

const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {

    module:{
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(svg|png|jpg|gif)$/,
                use:{
                    loader: 'file-loader',
                    options: {
                        name: '[name]-[hash].[ext]',
                        outputPath: 'img'
                    }
                }
            }
        ]
    },

    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            chunks: ['app'],
            minify: {
                removeAttributeQuotes: true
            }
        })
    ]
});